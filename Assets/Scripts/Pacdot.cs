﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pacdot : MonoBehaviour {

    public Text scoreText;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Pacman") {
            Score.score += 1;
            UpdateScore();
            Destroy(gameObject);
        }
    }

    private void UpdateScore() {
        scoreText.text = "Score : " + Score.score.ToString();
    }
}
