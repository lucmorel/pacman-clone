﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Transform seeker, target;
    public float chaseSpeed = 0.1f;

    private Grid grid;
    private List<Node> waypoints = new List<Node>();
    private Vector2 currentWaypoint;
    private Rigidbody2D seekerRB;

    private void Awake() {
        grid = GetComponent<Grid>();
        seekerRB = seeker.GetComponent<Rigidbody2D>();
        currentWaypoint = seeker.position;
    }

    private void Update() {
        FindPath(seeker.position, target.position);
        FollowPath();
    }

    private void FixedUpdate() {
        Vector2 step = Vector2.MoveTowards(seeker.position, currentWaypoint, chaseSpeed);
        seekerRB.MovePosition(step);
    }

    private void FindPath(Vector2 startPos, Vector2 targetPos) {
        Node startNode = grid.NodeFromWorldPosition(startPos);
        Node targetNode = grid.NodeFromWorldPosition(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        openSet.Add(startNode);

        while (openSet.Count > 0) {
            Node currentNode = openSet[0];
            for (int i = 0; i < openSet.Count; i++) {
                if (openSet[i].fCost < currentNode.fCost || (openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)) {
                    currentNode = openSet[i];
                }
            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == targetNode) {
                RetracePath(startNode, targetNode);
                return;
            }

            foreach (Node neighour in grid.GetNeighbours(currentNode)) {
                if (closedSet.Contains(neighour))
                    continue;

                int newGCost = currentNode.gCost + 1;
                if (newGCost < neighour.gCost || !openSet.Contains(neighour)) {
                    neighour.gCost = newGCost;
                    neighour.hCost = GetDistance(neighour, targetNode);
                    neighour.parent = currentNode;

                    if (!openSet.Contains(neighour))
                        openSet.Add(neighour);
                }
            }
        }
    }

    private void RetracePath(Node startNode, Node endNode) {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Reverse();

        grid.path = path;
        waypoints = path;
    }

    private int GetDistance(Node nodeA, Node nodeB) {
        return (Mathf.Abs(nodeA.gridPosX - nodeB.gridPosX) + Mathf.Abs(nodeA.gridPosY - nodeB.gridPosY));
    }

    private void FollowPath() {
        if (waypoints != null && (Vector2)seeker.position == currentWaypoint && currentWaypoint != (Vector2)target.position) {
            waypoints.RemoveAt(0);
            currentWaypoint = waypoints[0].position;
        }
    }
}
