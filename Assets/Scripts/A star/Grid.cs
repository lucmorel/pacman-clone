﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{

    public Vector2 gridSize;
    public float nodeRadius;
    public LayerMask unwalkableMask;

    private Node[,] grid;
    private float nodeDiameter;
    private int gridSizeX, gridSizeY;
    private Vector2 originOffset;

    private void Start() {
        nodeDiameter = nodeRadius * 2;

        gridSizeX = Mathf.RoundToInt(gridSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridSize.y / nodeDiameter);

        CreateGrid();

        if (grid != null)
            originOffset = Vector2.zero - grid[0, 0].position;
    }

    // This method draws on Gizmos to make the logic visualizable
    public List<Node> path;
    private void OnDrawGizmos() {
        // Gizmos.DrawWireCube(transform.position, new Vector3(gridSize.x, gridSize.y, 1));

        if (grid != null) {
            foreach (Node node in grid) {
                // Gizmos.color = (node.walkable) ? Color.white : Color.red;
                if (path != null && path.Contains(node)) {
                    Gizmos.color = Color.green;
                    Gizmos.DrawCube(node.position, Vector3.one * (nodeDiameter - 0.1f));
                }
            }
        }
    }

    // Create the grid where A* will be applied
    private void CreateGrid() {
        grid = new Node[gridSizeX, gridSizeY];
        Vector2 worldBottomLeft = (Vector2)transform.position + Vector2.left * gridSizeX / 2 + Vector2.down * gridSizeY / 2;

        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                Vector2 position = worldBottomLeft + Vector2.right * (x * nodeDiameter + nodeRadius) + Vector2.up * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics2D.OverlapCircle(position, nodeRadius, unwalkableMask));

                grid[x, y] = new Node(walkable, position, x, y);
            }
        }
    }

    public Node NodeFromWorldPosition(Vector2 worldPosition) {
        int x = Mathf.RoundToInt(worldPosition.x + originOffset.x);
        int y = Mathf.RoundToInt(worldPosition.y + originOffset.y);

        return grid[x, y];
    }

    public List<Node> GetNeighbours(Node node) {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0 || Mathf.Abs(x) + Mathf.Abs(y) == 2)
                    continue;

                int checkX = node.gridPosX + x;
                int checkY = node.gridPosY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY && grid[checkX, checkY].walkable)
                    neighbours.Add(grid[checkX, checkY]);
            }
        }

        return neighbours;
    }

}
