﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanControl : MonoBehaviour {

    public float Speed = 0.4f;
    public GameObject loosePanel;

    private Vector2 Destination = Vector2.zero;
    private Animator animator;
    private Rigidbody2D rigidBody;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start ()
    {
        Destination = transform.position;
	}
	
	void FixedUpdate ()
    {
        // Smoothly move to the destination each FixedUpdate according to Speed
        Vector2 step = Vector2.MoveTowards(transform.position, Destination, Speed);
        rigidBody.MovePosition(step);
	}

    void Update()
    {
        // Check for Inputs if not moving
        if ((Vector2)transform.position == Destination)
        {
            if (Input.GetKey(KeyCode.LeftArrow) && IsDirectionValid(Vector2.left))
                Destination = (Vector2)transform.position + Vector2.left;
            if (Input.GetKey(KeyCode.RightArrow) && IsDirectionValid(Vector2.right))
                Destination = (Vector2)transform.position + Vector2.right;
            if (Input.GetKey(KeyCode.UpArrow) && IsDirectionValid(Vector2.up))
                Destination = (Vector2)transform.position + Vector2.up;
            if (Input.GetKey(KeyCode.DownArrow) && IsDirectionValid(Vector2.down))
                Destination = (Vector2)transform.position + Vector2.down;
        }

        // Set the animation parameters that the state machine can change animations automatically
        Vector2 direction = Destination - (Vector2)transform.position;
        animator.SetFloat("DirX", direction.x);
        animator.SetFloat("DirY", direction.y);
    }

    // Check if Pacman can move in the direction 'dir'
    bool IsDirectionValid(Vector2 dir)
    {
        Vector2 pos = transform.position;
        // Cast line from pos+dir to Pacman
        RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);
        Debug.DrawRay(pos, dir, Color.green);
        
        // If we hit something else than Pacman, this is not a valid direction
        return (hit.collider == GetComponent<Collider2D>());
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Enemy") {
            Time.timeScale = 0;
            loosePanel.SetActive(true);
        }
    }
}
