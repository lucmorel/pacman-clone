﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public static int score = 0;
    public GameObject winPanel;

    private int maxScore;

    private void Awake() {
        maxScore = GameObject.Find("Dots").transform.childCount;
    }

    private void Update() {
        if (score >= maxScore) {
            Time.timeScale = 0;
            winPanel.SetActive(true);
        }
    }

}
