﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{

    public bool walkable;
    public Vector2 position;
    public int gridPosX;
    public int gridPosY;

    public Node parent;

    public int gCost = 0;
    public int hCost;
    public int fCost {
        get { return gCost + hCost; }
    }

    public Node(bool walkable, Vector2 position, int gridPosX, int gridPosY) {
        this.walkable = walkable;
        this.position = position;
        this.gridPosX = gridPosX;
        this.gridPosY = gridPosY;
    }

}
